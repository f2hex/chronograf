# ; -*- mode: dockerfile;-*-
FROM {{BUILD_VENDOR}}/{{TARCH}}-alpine:3.8

MAINTAINER Franco Fiorese <franco.fiorese@gmail.com>

# WARNING: at the moment the binary tarball of the project is not yet
# architecture idendependent

# Use a specific vesion of InfluxDB
ARG CHRONOGRAF_VERSION

# the location where to find a tarball package with InfluxDB binaries
# to be installed
ENV CHRONOGRAF_BINPLOC "https://dl.influxdata.com/chronograf/releases"

# set precedence of net name resolution when container is running
RUN echo 'hosts: files dns' >> /etc/nsswitch.conf

# ensure a proper availability of CA certificates (Mozilla Certificate
# Store)
RUN apk add --no-cache ca-certificates && \
    update-ca-certificates

# trap any error as an exit condition
RUN    set -e \
    && apk add --no-cache --virtual .build-deps wget gnupg tar \
    && for key in \
            05CE15085FC09D18E99EFB22684A14CF2582E0C5 ; \
       do \
           gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key" || \
           gpg --keyserver pgp.mit.edu --recv-keys "$key" || \
           gpg --keyserver keyserver.pgp.com --recv-keys "$key" ; \
       done \
    && cd /root \
    && wget --no-verbose ${CHRONOGRAF_BINPLOC}/chronograf-${CHRONOGRAF_VERSION}_linux_arm64.tar.gz.asc \
    && wget --no-verbose ${CHRONOGRAF_BINPLOC}/chronograf-${CHRONOGRAF_VERSION}_linux_arm64.tar.gz \
    && gpg --batch --verify chronograf-${CHRONOGRAF_VERSION}_linux_arm64.tar.gz.asc chronograf-${CHRONOGRAF_VERSION}_linux_arm64.tar.gz \
    && tar -xz -f chronograf-${CHRONOGRAF_VERSION}_linux_arm64.tar.gz \
    && chmod +x chronograf-*/usr/bin/* \
    && cp -a chronograf-*/usr/bin/* /usr/bin/ \
    && cp -a chronograf-*/usr/lib/* /usr/lib/ \
    && cp -a chronograf-*/usr/share/* /usr/share/ \
    && rm -rf chronograf-* .gnupg \
    && apk del .build-deps

COPY entrypoint.sh /entrypoint.sh
RUN  chmod +x  /entrypoint.sh

EXPOSE 8888

VOLUME /var/lib/chronograf

ENTRYPOINT ["/entrypoint.sh"]
CMD ["chronograf"]
